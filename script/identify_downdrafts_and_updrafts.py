#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : updrafts
# 13/06/2018
# N. Villefranque
# modif F Brient
# call : python identify_downdrafts.py downdraft SVT006 /cnrm/tropics/user/brientf/MESONH/FIRE/L25.6/L25.6.1.V0301.003.nc4

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np
import pylab as plt

if len(sys.argv)!=4 : 
    print "Usage : python",sys.argv[0], "object varsel ncfile"
    exit(1)

obj     = sys.argv[1] #'downdraft'
varsel  = sys.argv[2] #'SVT006'
ncfile  = sys.argv[3]

def findsigmaming(z,data,rang):
    # Find minimum threshold for tracers vertical evolution.
    # data : tracer anomaly
    # z is the altitude in meters 
    # Based on Couvreux et. al 10
    resSigma    = np.zeros(len(z))
    resSigmamin = np.zeros(len(z))
    dz=z[np.array(rang)+1]-z[rang]
    sigma=np.std(data[rang,:,:],axis=(1,2))
    integral=np.cumsum(sigma*dz)
    sigmamin = 0.05/z[rang]*integral
    resSigma[rang]=sigma
    resSigmamin[rang]=sigmamin
    return resSigma,resSigmamin

def anom(data):
    mean  = np.mean(data,axis=(1,2))
    mean  = np.tile(mean[:,None,None],(1,data.shape[1],data.shape[2]))
    data  = data-mean # anomalies
    return data

def zminmax():
    zmin = 0 ; zmax = 62
    return zmin,zmax

def suppzdata(thrs,zmin,zmax):
    # user function to remove some parts of the domain
    # by default used to remove anaomalies above the cloud-top
    if zmax is not None:
      hh   = np.ones(thrs.shape)*max(thrs)*3 # never happen
      hh[zmin:zmax]=thrs[zmin:zmax]
      thrs = hh
    return thrs

def selSVT(dictVars,mm) :
    # Find SVT and W in dictVars
    Wname = None
    SVT   = None
    for k in dictVars :
        if "SVT" in k : SVT=k
        if "W"   in k : Wname=k
    if SVT is not None :
      data  = dictVars[SVT]
    else :
      "No SVT var was found!" ; exit(1)
    if Wname is not None :
      W  = dictVars[Wname]

    data  = anom(data)
    mask  = data*0.
    z     = np.arange(data.shape[0])*50
    zmin,zmax = zminmax()
    rang  = range(1,len(z)-1)
    if SVT=='SVT006':
       rang = range(zmax,0,-1)
    elif SVT=='SVT003':
       zmax = len(z)-2
       rang = range(zmax,0,-1)
    sigma,sigmamin = findsigmaming(z,data,rang)
    if SVT=='SVT003':
    # add from bottom to top!
      rang  = range(1,len(z)-1)
      sigma0,sigmamin0 = findsigmaming(z,data,rang)
      sigmamin = np.maximum(sigmamin,sigmamin0)
 
    # first try : sigma in each layer
    thrs  = mm*np.maximum(sigma,sigmamin)
    thrs  = suppzdata(thrs,zmin,zmax)

    thrs[0]=max(thrs);thrs[-1]=max(thrs)
    thrs = np.tile(thrs[:,None,None],(1,data.shape[1],data.shape[2]))
    mask[data>thrs]=1 

    #  if double condition:
    if Wname is not None:
      # remove points that do not satisfy the WT sampling
      thrsW = 0.0
      thrsW = np.ones((W.shape))*thrsW
      if SVT=='SVT006' or SVT=='SVT003': # downdraft
        mask[~(W<thrsW)]=0
      elif SVT=='SVT004' or SVT=='SVT001': # updraft
        mask[~(W>thrsW)]=0

    return mask

def downdraftWT(dictVars,thrs) :
    w = dictVars['WT']
    mask=w*0.
    mask[w<thrs]=1 # cell is in updraft if vertical wind < thrs
    return mask

def updraftWT(dictVars,thrs) :
    w = dictVars['WT']
    mask=w*0.
    mask[w>thrs]=1 # cell is in updraft if vertical wind < thrs
    return mask

# manage cases with double condition
varsel2 = None
if '+' in varsel:
   varsel,varsel2 = varsel.split('+')

# obj is first arg on command line and varsel is the second one.
if "SVT" in varsel : 
   select = selSVT
   listThrs = [0,0.5,1,1.5,2] 

elif obj == 'downdraft' and varsel == 'WT':
   select   = downdraftWT
   listThrs=[0,-0.25,-0.5,-0.75,-1]

elif obj == 'updraft' and varsel == 'WT':
   select   = updraftWT
   listThrs=[0,0.25,0.5,0.75,1]

else : print "Wrong object:"+obj+" or wrong var"+varsel ; exit(1)

listVarsNames = [varsel]  # any size. The mask dimensions must be the same as first var in list
nameobj0      = obj[0:4]+"_"+varsel

if varsel2 is not None:
   listVarsNames += [varsel2]
   nameobj0      += "_"+varsel2

for ik,thrs in enumerate(listThrs):
  nameobj=nameobj0+"_%02i" %(ik)
  identify(ncfile, listVarsNames, select, argsCalcMask=(thrs,), name=nameobj,  write=True, rename=True, rmbounds=True,  overwrite=True)
