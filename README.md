# Objects
__This is a collaborative project to develop generic tools to identify and characterize objects in high resolution model output fields.__  
Git project creation 25/06/2018  
Authors : N. Villefranque 
          F. Brient  

## Description

### Main directory
The main repository contains documentation: this __README.md__ file, some useful git commands in __GIT.md__, a list of future developments in the __TODO__ file, and a tutorial presentation (in French) about the identification module, __tutoIdentify18012019.pdf__. It also contains a __src__ folder, an __example__ folder and a __script__ folder.

### src
The src folder contains the Python modules

 *identification_methods*
 where are implemented 
 - the function *identify*: 
>    identify(ncdfFile, listVarNames, funcCalcMask, argsCalcMask=(), dimMask=(), name="objects", diagonals=True, cyclic=(-2,-1), delete=0, rename=False, criteria=None, write=True, rmbounds=False,overwrite=False,unique=False)
 - the class *UserObject* with associated methods

To get more information use the command:
` python src/identification_methods.py `

 *characterization_methods*
 where are implemented
 - the function *characterize*
>    characterize(ncdfFile, listVarNames=[], dictFuncCalcChar=[], dictFuncUserVars=None, dimChars={0:'time'}, name="objects", cyclic=(-2,-1), write=True, ncdfOut=None)
 - the class *UserCharacs* with associated methods

To get more information use the command:
` python src/characterization_methods.py `

 *common_methods* 
 where are implemented functions to read and write netcdf files, compute statistics...

### examples
Examples of use can be found in the examples folder:
 - identify_updrafts.py
 - basic_identify_clouds.py 
 - argsMask_identify_clouds.py
 - dimMask_identify_clouds.py
 - cyclic_identify_clouds.py
 - track_clouds.py
 - track_clouds_2D.py
 - characterize_clouds.py
 - characterize_cloud_profiles.py

### script
A user script can be found in the script folder:
 - identification_downdraft_and_updraft.py

## Import the modules
To import the modules from the src folder: 
```
import sys  
sys.path.append('path/to/scripts_objects/src/') # to add the src folder in the PythonPath  
from identification_methods import identify # to import the identify function
from characterization_methods import characterize # to import the characterize function
```
