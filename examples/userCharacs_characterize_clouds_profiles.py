#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from characterization_methods import characterize
import numpy as np


ncfile="/home/villefranquen/Work/NCDF/LES3D/ARMCu/CERK4.1.ARMCu.008.nc"    
listVarsNames = ['RCT','THT','PABST','WT',
                 'S_N_direction','W_E_direction','VLEV'] 
  
def area(dictVars,indexes) :
  Xvec = dictVars["W_E_direction"]; dx = Xvec[1]-Xvec[0]
  Yvec = dictVars["S_N_direction"]; dy = Yvec[1]-Yvec[0]
  nbCloudCells = len(indexes[0])
  return 1.e6*dx*dy*nbCloudCells
  
characs = characterize(ncfile, listVarsNames,
          dictFuncCalcChar={'area km^2':area}, 
          dimChars={0:"time",1:"vertical_levels"})





