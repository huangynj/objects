#!/usr/bin/python
# -*- coding: latin-1 -*-
# test characterization_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from characterization_methods import characterize
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"
ncfile = "/home/villefranquen/Work/NCDF/LES3D/ARMCu/CERK4.1.ARMCu.008.nc"

listVarsNames = ['RCT','THT','PABST','WT',
                 'S_N_direction','W_E_direction','VLEV'] 

def volume(dictVars,indexes) :
  Xvec = dictVars["W_E_direction"]; dx = Xvec[1]-Xvec[0]
  Yvec = dictVars["S_N_direction"]; dy = Yvec[1]-Yvec[0]
  Zvec = dictVars["VLEV"][:,0,0]  ; dz = Zvec[1]-Zvec[0]
  nbcells = len(indexes[0])
  volume_cloud=nbcells*dx*dy*dz*1e9
  return volume_cloud

def area(dictVars,indexes) :
  Xvec = dictVars["W_E_direction"]; dx = Xvec[1]-Xvec[0]
  Yvec = dictVars["S_N_direction"]; dy = Yvec[1]-Yvec[0]
  uniqueCloudLevs = np.unique(indexes[0])
  allCloudLevs = indexes[0]
  nbCells_atLevel = [len(allCloudLevs[allCloudLevs==lev]) 
                     for lev in uniqueCloudLevs]
  return 1.e6*dx*dy*np.array(nbCells_atLevel)

characs = characterize(ncfile, listVarsNames, 
          dictFuncCalcChar={'volume m^3':volume,'area m^2':area})



