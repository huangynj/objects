#!/usr/bin/python
# -*- coding: latin-1 -*-
# test characterization_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from characterization_methods import characterize
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"

listVarsNames = ['W_E_direction','S_N_direction','VLEV','RCT','THT','PABST'] 
ra = 287.
cp = 1004.
ra_over_cp = ra/cp
def ke(dictVars) :
  rc = dictVars['RCT']
  th = dictVars['THT']
  pr = dictVars['PABST']
  p0 = pr[:,0,:,:]
  den = ra*th*p0**(-ra_over_cp)
  rho = pr**(1.-ra_over_cp)/den
  lwc = rc*rho
  rho_w = 1000.
  r_eff = 10.0e-9
  ke = 3./2.*lwc/(r_eff*rho_w)
  return ke
  
def area(dictVars,indexes) :
  iz = indexes[0]
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  return [len(iz[iz==z])*dx*dy for z in np.unique(iz)]
  
def volume(dictVars,indexes) :
  xx = dictVars['W_E_direction']
  yy = dictVars['S_N_direction']
  zz = dictVars['VLEV'][:,0,0]
  dx = xx[1]-xx[0]
  dy = yy[1]-yy[0]
  dz = zz[1]-zz[0]
  return len(indexes[0])*dx*dy*dz

characs = characterize(ncfile, listVarsNames, dictFuncUserVars={'extinction_coefficient km^-1':ke}, name="test_clouds",dictFuncCalcChar={'area km^2':area,'volume km^3':volume})
