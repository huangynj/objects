#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"

listVarsNames = ['RCT'] # list of relevant variables.

def clouds(dictVars) :
    rc = dictVars['RCT']        # original shape is t,z,y,x
    newshape = (rc.shape[0],rc.shape[2],rc.shape[3])
    mask=np.zeros(newshape)     # new shape is t,y,x
    mask[np.sum(rc,axis=1)>0]=1 # cell is in cloud if liquid water path is positive
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, clouds, cyclic=(-1,),
                 dimMask=(0,2,3), rename=True, name="cloud_image_openY")








