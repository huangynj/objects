#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : track coldpools
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import matplotlib.pyplot as plt
import numpy as np

ncfile = "/home/villefranquen/SIMULATIONS/BOMEX_LES/RDFRC/RDFRC.1.BOMEX.ALL.nc4"
ncfile = "/home/villefranquen/SIMULATIONS/ARM_LES/REFHR/z_extracted_REFHR.1.ARMCu.4D.nc" # Whole 4D field too big => memory error
ncfile = "../../data/CERK4.1.ARMCu.008.nc"

listVarsNames = ['THT','RVT'] # any size. The mask dimensions must be the same as first var in list

def coldpools(dictVars,alpha) :
    theta = dictVars['THT']
    qv = dictVars['RVT']
    thetav = theta*(1. + 0.61*qv)
    if len(thetav.shape)==3: ano_thetav = thetav - np.mean(thetav,axis=(-1,-2))[:,None,None] ; plt.imshow(ano_thetav[0,:,:],origin="lower")
    elif len(thetav.shape)==4: 
        ano_thetav = thetav - np.mean(thetav,axis=(-1,-2))[:,:,None,None] ;
        for i in range(thetav.shape[0]) : plt.imshow(ano_thetav[i,0,:,:],origin="lower"); plt.savefig("ano_"+str(alpha)+str(i)+".png")
    mask=qv*0.   # same shape as THVT
    mask[ano_thetav<alpha]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
for i,seuil in enumerate(np.linspace(-2,0,11,endpoint=True)) :
  print i,seuil
  objects, types = identify(ncfile, listVarsNames, coldpools, argsCalcMask=(seuil), name="coldpools_"+str(i),  write=True, overwrite=True)
