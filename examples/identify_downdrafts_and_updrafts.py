#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : updrafts
# 13/06/2018
# N. Villefranque
# modif F Brient
# call : python identify_downdrafts.py downdraft SVT006 /cnrm/tropics/user/brientf/MESONH/FIRE/L25.6/L25.6.1.V0301.003.nc4

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np
import pylab as plt


def format(value):
    return "%10.4f" % value

# Anomalies 3D relative to 2D average
def anom(data):
    mean  = np.mean(data,axis=(1,2))
    mean  = np.tile(mean[:,None,None],(1,data.shape[1],data.shape[2]))
    data  = data-mean # anomalies
    return data

# Find nearrest point 
def near(array,value):
  idx=(abs(array-value)).argmin()
  return idx

def zminmax(z,minmax):
    if minmax is not None:
      zmin = near(z,minmax[0]) ; zmax = near(z,minmax[1])
    else:
      zmin = 0; zmax = len(z)
    return zmin,zmax

def suppzdata(thrs,zmin,zmax):
    # user function to remove some parts of the domain
    # by default used to remove anomalies above the cloud-top
    if zmax is not None:
      hh   = np.ones(thrs.shape)*max(thrs)*3 # never happen
      hh[zmin:zmax]=thrs[zmin:zmax]
      thrs = hh
    return thrs


def findsigmaming(z,zdemi,data,rang):
    # Find minimum threshold for tracers vertical evolution.
    # z     is the altitude in meters (middle of layers)
    # zdemi is the altitude in meters (interlayers)
    # data  is the 3D tracer anomaly
    # rang  are indexes over which sigma are calculated
    # Based on Couvreux et. al (10)

    # Output, sigma and sigma_min
    resSigma    = np.zeros(len(zdemi))
    resSigmamin = np.zeros(len(zdemi))

    dz          = z[np.array(rang)+1]-z[rang]
    sigma       = np.std(data[rang,:,:],axis=(1,2))
    integral    = np.cumsum(sigma*dz)
    sigmamin    = 0.05/zdemi[rang]*integral

    resSigma[rang]    = sigma
    resSigmamin[rang] = sigmamin

    #plt.plot(resSigma);plt.plot(resSigmamin);plt.show()
    return resSigma,resSigmamin

def findbasetop(data,epsilon):
    # Find cloud base and cloud top
    # Define as the first layer where RCT (ql) > epsilon
    cloudbase = 0; cloudtop = 0;
    for ij in range(len(data)):
      if cloudbase == 0 and data[ij] > epsilon:
         cloudbase = ij
      if cloudbase != 0  and cloudtop == 0  and data[ij] < epsilon:
         cloudtop  = ij
    #print 'base,top : ',cloudbase,cloudtop,data*1000.
    return cloudbase,cloudtop

def cloudinfo(rct,epsilon):
    # Find cloud base and cloud top and middle of cloud

    # First computation : Use the mean 
    meanql  = np.mean(np.mean(rct,axis=2),axis=1)
    cloudbase,cloudtop = findbasetop(meanql,epsilon)

    # Second computation : Each vertical layer
    ny,nx = rct.shape[1],rct.shape[2]
    cloudbase0 = np.zeros((ny,nx))
    cloudtop0  = np.zeros((ny,nx))
    for ix in range(nx):
      for iy in range(ny):
         data = rct[:,iy,ix]
         cloudbase0[iy,ix],cloudtop0[iy,ix] = findbasetop(data,epsilon)     

    cloudbase2 = np.nanmean(cloudbase0[:]) # real
    cloudtop2  = np.nanmean(cloudtop0[:]) # real

    #cloudtop3  = np.nanmax(cloudtop0[:]) # real
    #print cloudtop,cloudtop2,cloudtop3

    # Make integer
    cloudbase  = int(cloudbase2)
    cloudtop   = int(cloudtop2)+1
    
    cloudmiddle  = int(round((cloudbase+cloudtop)/2))
    print 'Compute final :'
    print 'cloudbase : ij='+str(cloudbase)
    print 'cloudtop  : ij='+str(cloudtop)
    return cloudbase,cloudmiddle,cloudtop,cloudbase0,cloudtop0


def selSVT(dictVars,mm) :
    # Find SVT and W in dictVars
    Wname = None
    SVT   = None
    for k in dictVars :
      if "SVT" in k : SVT=k
      if "W"   in k : Wname=k
      if "RVT" in k : SVT=k

    if SVT is not None :
      data0  = dictVars[SVT]
    else :
      "No SVT var was found!" ; exit(1)

    if Wname is not None :
      W  = dictVars[Wname]

    data  = anom(data0)
    mask  = data*0.
    z     = dictVars["ZHAT"]

    # inter-layers
    zdemi = np.array([(z[ij]+z[ij+1])/2.0 for ij in range(len(z)-1)])
    zdemi = np.append(zdemi,z[-1]+(z[-1]-z[-2])/2.)

    # Tracer indetification end or start at zmin
    zmin  = 0

    # cloud-top tracer
    if SVT=='SVT003' or SVT=='SVT006':

      if 'RCT' in dictVars.keys():
        rct     = dictVars['RCT']
      else:
        rct     = False

      if rct:
        # Define as the first layer where RCT (ql) > epsilon
        # 1e-6 by default
        epsilon = 1e-6
        cloudbase,cloudmiddle,cloudtop,zb,zi = cloudinfo(rct,epsilon)
        print 'Cloud : ',cloudbase,cloudmiddle,cloudtop#,zb,zi
      else:
        cloudtop=0

      # Test : altitude of emission = max SVT
      # This means that cloud top is actually not use here, only the layer wherein the max of SVT lies on.
      mean   = np.nanmean(data0,axis=(1,2))
      idxsvt = np.argmax(mean)
      
      #Cloud-top tracer emission : cloud top + Ze
      Ze    = idxsvt-cloudtop #indexes quantifying the difference between cloud top and max SVT
      # Ze can be imposed herebelow if wanted

      zmax  = cloudtop+Ze
      rang  = range(zmax,-1,-1)
    elif SVT=='RVT':
      zmax  = len(zdemi)-2
      rang  = range(zmax,zmin,-1)
    else: # default
      zmax  = len(zdemi)-1
      rang  = range(zmin,zmax)

    # Find sigma based on Couvreux et. al (10)
    sigma,sigmamin = findsigmaming(z,zdemi,data,rang)
    
      
    #fileout = 'write_sigma.txt'; 
    #tab     = np.array((zdemi,sigma,sigmamin))
    #print tab.shape
    #f = open(fileout, 'wb')
    #for x,y in enumerate(tab.T):
      #print x,y
    #  [f.write(str(format(yy))+' ') for yy in y]
    #  f.write("\n")
    #f.close()

    # first try : sigma in each layer
    thrs  = mm*np.maximum(sigma,sigmamin)
    
    # Remove sampling above zmax, and below zmin
    thrs  = suppzdata(thrs,zmin,zmax)

    # Make 3D table
    thrs = np.tile(thrs[:,None,None],(1,data.shape[1],data.shape[2]))

    # Create mask
    if mm<0:
       mask[data<thrs]=1
    else:
       mask[data>thrs]=1
    
    # Double condition
    # Can be improved
    if Wname is not None:
      # remove points that do not satisfy the WT sampling
      thrsW = 0.0
      thrsW = np.ones((W.shape))*thrsW
      if SVT=='SVT006' or SVT=='SVT003': # downdraft
        mask[~(W<thrsW)]=0
      elif SVT=='SVT004' or SVT=='SVT001': # updraft
        mask[~(W>thrsW)]=0

    return mask

def downdraftWT(dictVars,thrs) :
    w = dictVars['WT']
    mask=w*0.
    mask[w<thrs]=1 # cell is in updraft if vertical wind < thrs
    return mask

def updraftWT(dictVars,thrs) :
    w = dictVars['WT']
    mask=w*0.
    mask[w>thrs]=1 # cell is in updraft if vertical wind < thrs
    return mask


if len(sys.argv)!=4 : 
    print "Usage : python",sys.argv[0], "object varsel ncfile"
    exit(1)

obj     = sys.argv[1] # Typ of motion, e.g. 'downdraft'
varsel  = sys.argv[2] # Typ of tracer, e.g. 'SVT006'
ncfile  = sys.argv[3] # Name of the file : path + filename

# manage cases with double condition
varsel2 = None
if '+' in varsel:
   varsel,varsel2 = varsel.split('+')

# By default: Selected by tracer concentrations
select   = selSVT
# By default: Conditional sampling scale from m=0 to m=2
listThrs = [0,0.5,1,1.5,2] 

if obj == 'downdraft':
  if varsel == 'WT':
    select   = downdraftWT
    listThrs=[0,-0.25,-0.5,-0.75,-1]
  elif varsel == 'RVT':
    listThrs = [-2,-1.5,-1,-0.5,0] 
elif obj == 'updraft':
  if varsel == 'WT':
    select   = updraftWT
    listThrs=[0,0.25,0.5,0.75,1]
  elif varsel == 'RVT':
    listThrs = [0,0.5,1,1.5,2] 
else : 
  print "Wrong object:"+obj+" or wrong var"+varsel ; exit(1)


listVarsNames = [varsel,"ZHAT"]  # any size. The mask dimensions must be the same as first var in list
nameobj0      = obj[0:4]+"_"+varsel

if varsel2 is not None:
   listVarsNames += [varsel2]
   nameobj0      += "_"+varsel2


for ik,thrs in enumerate(listThrs):
  nameobj=nameobj0+"_%02i" %(ik)
  identify(ncfile, listVarsNames, select, argsCalcMask=(thrs,), name=nameobj,  rename=True, rmbounds=True,  overwrite=True)



