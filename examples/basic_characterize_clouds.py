#!/usr/bin/python
# -*- coding: latin-1 -*-
# test characterization_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from characterization_methods import characterize
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"

listVarsNames = ['RCT','THT','PABST','WT'] 

characs = characterize(ncfile, listVarsNames)

