#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"

listVarsNames = ['RCT','WT'] # list of relevant variables.

def clouds(dictVars,threshold1,threshold2) :
    rc = dictVars['RCT']
    wt = dictVars['WT']
    mask=rc*0.   # same shape as RCT
    mask[(rc>threshold1) & (wt>threshold2)]=1 
    # cell is in cloud if liquid water mixing ratio 
    # is greater than a threshold1 and if vertical
    # wind is greater than a threshold2
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, clouds, argsCalcMask=(1.e-6,0.),
                          name="active_clouds")

