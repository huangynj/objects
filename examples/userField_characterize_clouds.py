#!/usr/bin/python
# -*- coding: latin-1 -*-
# test characterization_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from characterization_methods import characterize
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"
ncfile = "/home/villefranquen/Work/NCDF/LES3D/ARMCu/CERK4.1.ARMCu.008.nc"

listVarsNames = ['RCT','THT','PABST','WT'] 
ra = 287.
cp = 1004.
ra_over_cp = ra/cp
rho_w = 1000.
r_eff = 10.0e-9
def ke(dictVars) :
  rc = dictVars['RCT']; th = dictVars['THT']; pr = dictVars['PABST']
  p0 = pr[:,0,:,:]
  den = ra*th*p0**(-ra_over_cp)
  rho = pr**(1.-ra_over_cp)/den
  ke = 3./2.*rc*rho/(r_eff*rho_w)
  return ke

characs = characterize(ncfile, listVarsNames, 
          dictFuncUserVars={'extinction_coefficient km^-1':ke})



