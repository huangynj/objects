#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : track clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"
ncfile = "/home/villefranquen/SIMULATIONS/BOMEX_LES/RDFRC/RDFRC.1.BOMEX.ALL.nc4"

listVarsNames = ['RCT'] # any size. The mask dimensions must be the same as first var in list


def clouds(dictVars) :
    rc = dictVars['RCT']
    tmp=rc*0.   # same shape as RCT
    tmp[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    mask = np.sum(tmp, axis=1)
    mask[mask>0]=1
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, clouds, dimMask=(0,2,3), name="track_clouds_2D",  write=True)
