#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncdfFile = "../../data/CERK4.1.ARMCu.008.nc"

listVarNames = ['RCT'] # list of relevant variables.

def cloudMask(dictVars) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>0]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, tmp = identify(ncdfFile, listVarNames, cloudMask)








