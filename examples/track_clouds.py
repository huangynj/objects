#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : track clouds
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"
ncfile = "/home/villefranquen/SIMULATIONS/BOMEX_LES/RDFRC/RDFRC.1.BOMEX.ALL.nc4"
ncfile = "/home/villefranquen/SIMULATIONS/ARM_LES/REFHR/z_extracted_REFHR.1.ARMCu.4D.nc" # Whole 4D field too big => memory error

listVarsNames = ['RCT'] # any size. The mask dimensions must be the same as first var in list


def clouds(dictVars) :
    rc = dictVars['RCT']
    mask=rc*0.   # same shape as RCT
    mask[rc>1.e-6]=1 # cell is in cloud if liquid water mixing ratio is positive
    return mask

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, clouds, name="track_clouds",  write=True)
