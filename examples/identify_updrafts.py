#!/usr/bin/python
# -*- coding: latin-1 -*-
# test identification_methods.py : updrafts
# 13/06/2018
# N. Villefranque

import os,sys
sys.path.append('../src/')
from identification_methods import identify
import numpy as np

ncfile = "../../data/CERK4.1.ARMCu.008.nc"

listVarsNames = ['WT','RCT']  # any size. The mask dimensions must be the same as first var in list

def updrafts(dictVars) :
    w = dictVars['WT']
    mask=w*0.
    mask[w>2.5]=1 # cell is in updraft if vertical wind > 2.5m/s
    return mask

def criteria(dictVars, indsObj) :
    t,z,y,x=(indsObj)
    rc = dictVars['RCT'] 
    if any(rc[t,z,y,x]>0) : return 1 # saturated updraft
    else : return 2

# To read more info on the function identify, uncomment the following line
#help(identify)
objects, types = identify(ncfile, listVarsNames, updrafts, name="test_updrafts", delete=16,  write=True, criteria=criteria)
