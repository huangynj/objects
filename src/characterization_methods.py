#!/usr/bin/python
# -*- coding: latin-1 -*-
# Adapted from previous script 
# Now object oriented and user generic 
# 25/06/2018
# N. Villefranque

import netCDF4 as ncdf
import numpy as np
import scipy as sp
import time, datetime
from common_methods import *

# When computing statistics, we won't always want to average over all dimensions, e.g. time or vertical levels
# If we do not average, then we need to define this dimension in the output netcdf file
# Dimensions in the output should go as [time, [vlev,]] object, stat
# Arbitrary choices: if time exists, never average over time
#                    with a vlev flag in the main function: for vars and userVars, write profiles
#                    with a vlev flag for each calcChar: write profile

def characterize(ncdfFile, listVarNames=[], dictFuncCalcChar=[], dictFuncUserVars=None, dimChars={0:'time'}, name="objects", cyclic=(-2,-1), write=True, ncdfOut=None):
  """characterizes user defined objects with several mandatory and optional arguments
  mandatory arguments :
      - ncdfFile : name of netCDF file containing user data, in particular the field with the objects labels.
  optional arguments :
      - listVarNames : a list of strings, of any size, containing the names of the fields needed to characterize objects. They will be passed to user functions. Statistics of each of these variables will be computed for each object, if the variable shape is the same as the objects field.
      - dictFuncCalcChar : a list of user defined functions to compute diagnoses used to characterize objects (e.g mass flux, area) which arguments are the vars in listVarNames and the indexes of the cells of the current object. Such functions must return a value or a list of values, from which statistics will be computed.
      - dictFuncUserVars : a list of user defined functions to compute fields (e.g coefficient extinctions) which arguments are the vars in listVarNames. Such functions must return a field of the same shape of the objects field. Statistics of this variable will be computed for each object.
      - dimChars : a dictionnary to specify which dimensions should not be considered for averaging and other statistical diagnosis (default is the first dimension, time). The key is the index of the dimension in objects.shape and the value is the name of the dimension to write in the output file.
      - name : "objects" (default) or a string to find the object field in the netCDF file.
      - cyclic : None or a tuple to identify the cyclic dimensions (default are the last 2 dimensions).
      - write : True (default) or False, to write the computed fields in a netCDF file.
      - ncdfOut : None (default) or a string giving the name of the new netCDF file containing the characteristics (default is ncdfFile_charac.nc).
  """
  t0 = time.time()
  print 'Begin object characterization'

  t1 = time.time()
  print 'Reading',listVarNames
  dictVars,dims = read(ncdfFile, [name]+listVarNames)
  print len(dictVars)
  print '...OK (%2.2fs)' %(time.time()-t1)

  if not isinstance(dictFuncUserVars,type(None)):
      t1 = time.time()
      print 'Computing user variables'
      dictVars = addUserVars(dictVars, dictFuncUserVars)
      print '...OK (%2.2fs)' %(time.time()-t1)

  t1 = time.time()
  print 'Characterizing each object'
  out = UserCharacs(name,dictVars,dictFuncCalcChar,dimChars)
  print '...OK (%2.2fs)' %(time.time()-t1)

  if write :
   t1 = time.time()
   if isinstance(ncdfOut,type(None)): ncdfOut=ncdfFile.split('.nc')[0]+'_charac.nc'
   print 'Writing characs to',ncdfOut
   out.write(ncdfOut)

  print 'Done. (%2.2fs)' %(time.time()-t0)
  return out.characs

class UserCharacs:
    """ This class contains methods to define and compute statistical characterizations of objects"""
    def __init__(self, objName, dictVars, dictFunc, dimChars) :
      self.objects   = dictVars[objName]
      #     self.objects   = np.squeeze(self.objects) # without dimensions of size 1
      self.listObjs  = np.unique(self.objects[self.objects!=0])
      self.nbObjs    = len(self.listObjs)
      self.dims      = self.objects.shape
      self.dim       = len(self.dims)
      if dictFunc is None: dictFunc=[]
      self.nbCharacs = len(dictVars)+len(dictFunc)
      self.dictFuncs = dictFunc
      self.dictVars  = dictVars
      self.nbStats   = 7 # mean, min, max, std, median, 5th percentile, 95th percentile
      self.dimChars  = dimChars
      self.characs = self.characs()
 
    def characs(self):
      # let's assume the max dimension of the characteristics is 
      # (d0, d1, object, stat)
      n0=1
      n1=1
      nk=0
      if len(self.dimChars) >= 1 :
        n0 = self.dims[list(self.dimChars)[0]]
      if len(self.dimChars) >= 2 :
        n1 = self.dims[list(self.dimChars)[1]]
      dimTup = (self.nbCharacs, n0, n1, self.nbObjs, self.nbStats)
      characs = np.zeros(dimTup)
      thisObjs = self.objects
      for i0 in range(n0):
        for i1 in range(n1):
          if len(self.dimChars)>=1 : thisObjs = np.take(thisObjs,i0,axis=list(self.dimChars)[0])
          if len(self.dimChars)>=2 :
              if list(self.dimChars)[0]>list(self.dimChars)[1] : thisObjs = np.take(thisObjs,i1,axis=list(self.dimChars)[1])
              else : thisObjs = np.take(thisObjs,i1,axis=list(self.dimChars)[1]-1)
          for i2,obj in enumerate(self.listObjs):
            if obj in np.unique(thisObjs):
              for k, func in enumerate(self.dictFuncs):
                characs[k,i0,i1,i2,:] = stats(self.dictFuncs[func](self.dictVars,np.where(thisObjs==obj)))
                nk=k+1
              for k, field in enumerate(self.dictVars):
                if self.dictVars[field].shape == self.dims:
                  thisField = self.dictVars[field]
                  if len(self.dimChars)>=1 : thisField = np.take(thisField,i0,axis=list(self.dimChars)[0])
                  if len(self.dimChars)>=2 : 
                      if list(self.dimChars)[0]>list(self.dimChars)[1] : thisField = np.take(thisField,i1,axis=list(self.dimChars)[1])
                      else : thisField = np.take(thisField,i1,axis=list(self.dimChars)[1]-1)
                  characs[nk+k,i0,i1,i2,:] = stats(thisField[np.where(thisObjs==obj)])
          thisObjs = self.objects
      return characs

    def write(self,ncdfOut) :
      dset = ncdf.Dataset(ncdfOut,'w')
      # Define dimensions
      dset.createDimension('stats',self.nbStats)
      dset.createDimension('object',self.nbObjs)
      dset.description = "Statistical characterization of objects population. Stats are 1. mean, 2. min, 3. max, 4. std, 5. median, 6. 5th percentile, 7. 95th percentile."
      dimChars = ('object','stats',)
      for i in range(len(self.dimChars)):
          ndim = list(self.dimChars)[::-1][i]
          dset.createDimension(self.dimChars[ndim],self.dims[ndim])
          dimChars = (self.dimChars[ndim],)+dimChars
      # For each charac, create a new field and write charac
      nk=0
      for k,func in enumerate(self.dictFuncs):
          nk=k+1
          var=dset.createVariable(func.split(' ')[0], 'f8', dimChars)
          var[:] = np.take(self.characs, k, axis=0)
          if len(func.split(' '))>1 : var.units=func.split(' ')[1]
      for k,field in enumerate(self.dictVars):
          var=dset.createVariable(field.split(' ')[0], 'f8', dimChars)
          var[:] = np.take(self.characs, nk+k, axis=0)
          if len(field.split(' '))>1 : var.units=field.split(' ')[1]
      dset.close()

if __name__=="__main__":
    help("characterization_methods")
