#!/usr/bin/python
# -*- coding: latin-1 -*-
# 25/06/2018
# N. Villefranque

import netCDF4 as ncdf
import numpy   as np
from collections import OrderedDict

def removebounds(tmp):
   if len(tmp.shape) == 1.:
     tmp = tmp[1:-1]
   elif len(tmp.shape) == 3.:
     tmp = tmp[1:-1,1:-1,1:-1]
   else:
     print 'Problem with removebounds'
     exit(1)
   return tmp

def addbounds(tmp):
   ss   = [ij+2 for ij in tmp.shape]
   tmp2 = np.zeros(ss)*np.nan
   if len(tmp.shape) == 1.:
     tmp2[1:-1] = tmp
   elif len(tmp.shape) == 3.:
     tmp2[1:-1,1:-1,1:-1] = tmp
   else:
     print 'Problem with addbounds'
     exit(1)
   return tmp2


def check_name_in_ncdf(myname,ncvars,ncdfFile,overwrite=False) :
    name = myname
    while(myname in ncvars) :
        print '\t', myname, 'already exists in', ncdfFile
        if not overwrite:
          name = raw_input('\toverwrite (o), rename (type new name), exit (e) ?\n')
        else:
          name = 'o'
        if name=='o' : break
        elif name=='e' : exit()
        else : myname = name
    return name

def read(ncdfFile, listVarNames, dimMask=(), rmbounds=False) :
  dset = ncdf.Dataset(ncdfFile, 'r')
  dictVars = OrderedDict()
  if isinstance(listVarNames, type(None)) : raise TypeError('listVarNames is empty')
  if isinstance(listVarNames, str) : listVarNames = list(listVarNames)
  for i,n in enumerate(listVarNames) :
    v = dset.variables[n]
    if i==0:
      if len(dimMask)==0: dims = v.dimensions
      else : dims=tuple([v.dimensions[d] for d in dimMask])
    if rmbounds:
      v = removebounds(v)
    dictVars[n]=v[:].reshape(v.shape)
  dset.close()
  return dictVars,dims

def stats(tab):
    return [np.mean(tab),np.min(tab),np.max(tab),np.std(tab),np.median(tab),np.percentile(tab,5),np.percentile(tab,95)]

def addUserVars(dictVars,dictFuncUserVars):
    for n in dictFuncUserVars:
        dictVars[n]=dictFuncUserVars[n](dictVars)
    return dictVars
